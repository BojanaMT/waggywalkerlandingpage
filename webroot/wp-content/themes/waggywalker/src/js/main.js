(function($) {
  if ( $(window).width() < 768) {
      $('.nav a').on('click', function(e) {
          $('.nav').removeClass('show');
      }),
      $('.burger').on('click', function(){
          $('.nav').toggleClass('show');
      })
  } 
})( jQuery );

(function($) {
    $('.menu-item-10').on('click', function() {
        $('html, body').animate({
          scrollTop: $(".hero").offset().top
        }, 1000)
      }),
    $('.menu-item-27').on('click', function() {
        $('html, body').animate({
          scrollTop: $(".services").offset().top
        }, 1000)
      }),   
    $('.menu-item-28').on('click', function() {
        $('html, body').animate({
          scrollTop: $(".dog-walker").offset().top
        }, 1000)
      }), 
    $('.menu-item-29').on('click', function() {
        $('html, body').animate({
          scrollTop: $(".dog-owners").offset().top
        }, 1000)
      }), 
    $('.menu-item-30').on('click', function() {
        $('html, body').animate({
          scrollTop: $(".about").offset().top
        }, 1000)
      }), 
    $('.menu-item-31').on('click', function() {
        $('html, body').animate({
          scrollTop: $(".faq").offset().top
        }, 1000)
      })  
})( jQuery );


// services
// walkers
// customers
// about
// faq