<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WaggyWalker
 */

?>
	<footer class="footer">
		<div class="footer__content">	
			<div class="footer__social"> 	
				<a class="footer__logo-link" href="#">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/Logo.png"  alt="logos" class="footer__logo-img" />
				</a>
				<ul class="footer__social-icons">
					<li class="footer__social-icon"><a class="footer__social-icon--facebook" href="https://facebook.com"></a></li>
					<li class="footer__social-icon"><a class="footer__social-icon--instagram" href="https://www.instagram.com"></a></li>
					<li class="footer__social-icon"><a class="footer__social-icon--pinterest" href="https://www.pinterest.com/"></a></li>
					<li class="footer__social-icon"><a class="footer__social-icon--linkedin" href="https://www.linkedin.com"></a><li>
				</ul>
			</div>
			<p class="footer__copyright"> Copyright &copy; 2019 WaggyWalker</p>	
		</div>
	</footer>
	</div><!-- #content -->


<?php wp_footer(); ?>

</body>
</html>
