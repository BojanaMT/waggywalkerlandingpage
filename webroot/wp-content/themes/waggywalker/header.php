<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WaggyWalker
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	
	<header id="masthead" class="header">
		<div class="header__container">
		<a class="header__logo-link" href="#">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/Logo.png"  alt="logos" class="header__logo-img" />
		</a>

		<nav id="site-navigation" class="header__navigation">
			<button class="burger">
				<span class="burger__line burger--first"></span>
				<span class="burger__line burger--second"></span>
				<span class="burger__line burger--third"></span>
			</button>
			<?php
			wp_nav_menu( array(
				'theme_location' => 'menu-1',	
				'container_class' => 'nav',
				'menu_class' => 'nav__menu',
			) );
			?>
		</nav><!-- #site-navigation -->
			</div>
	</header><!-- #masthead -->

	<div id="content" class="site-content">
