<?php
/**
 * Plugin Name:     Waggy Walker Tools
 * Plugin URI:      PLUGIN SITE HERE
 * Description:     PLUGIN DESCRIPTION HERE
 * Author:          YOUR NAME HERE
 * Author URI:      YOUR SITE HERE
 * Text Domain:     ww_tools
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         Ww_tools
 */

// Your code starts here.

function ww_sign_up__sign_up__enqueue_scripts() {
	wp_enqueue_style( 'sign_up_css', plugins_url( '/css/sign_up.css', __FILE__ ), false, '0.1', 'all' );
	wp_enqueue_script( 'sign_up_js', plugins_url( '/js/sign_up.js', __FILE__ ), [ 'jquery' ], '0.1', true );
}

add_action( 'wp_enqueue_scripts', 'ww_sign_up__sign_up__enqueue_scripts' );

function ww_sign_up() {
	return "
		<div class='sign-up'>
			<input type='email' required placeholder='Email' id='ww_tools__sign_up__input' class='sign-up__field'/>
			<button id='ww_tools__sign_up__submit' class='sign-up__submit' onclick='ww_tools_sign_up()' disabled='disabled'>Submit</button>
		</div>
	";
}

add_shortcode( 'ww_sign_up', 'ww_sign_up' );
