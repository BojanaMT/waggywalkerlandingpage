/**
 * Sign Up
 */
function ww_tools_sign_up() {
	var email = document.getElementById('ww_tools__sign_up__input').value;
	var url = `http://waggywalker.devbox21.com/register?email=${email}`;
	var win = window.open(url, '_blank');
	win.focus();
}

/**
 * HELPER | Validate Email
 *
 * @param email
 * @returns {boolean}
 *
 * @docs https://stackoverflow.com/a/44671643/9753035
 */
function ww_tools_validate_email(email) {
	return /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()\.,;\s@\"]+\.{0,1})+([^<>()\.,;:\s@\"]{2,}|[\d\.]+))$/.test(email);
}

/**
 * Form Validate
 *
 * @param e
 */
function ww_tools_sign_up_validate(e) {

	var email = e.target.value;
	var valid = ww_tools_validate_email(email)

	var submit = document.getElementById('ww_tools__sign_up__submit');
	if (valid) submit.removeAttribute('disabled');
	else submit.setAttribute("disabled", "disabled");
}
// Event Listener for keyUp
document.getElementById('ww_tools__sign_up__input').addEventListener("keyup", ww_tools_sign_up_validate);