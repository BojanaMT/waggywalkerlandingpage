<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'wordpress' );

/** MySQL database password */
define( 'DB_PASSWORD', 'wordpress' );

/** MySQL hostname */
define( 'DB_HOST', 'mariadb' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'rr2L9uca<{/NBO}]*}|#;TADQ!Nf(pV=/.Z!YI_p(ak? GLFS#&D.x[8y^`];Fzh' );
define( 'SECURE_AUTH_KEY',  'fX^0% n&Quc%.m_7J=8>|$(*2~VVKW4H#hEErfa_m2n7(KP!w76MZ1T[nUS i`&k' );
define( 'LOGGED_IN_KEY',    't&=|)Go~>#/D7TbsmiYA85*q7BQ;l+RR#,G`!IH1-u$t08djjr~CNKwu8/ddo}V/' );
define( 'NONCE_KEY',        'Jxr@ %2+?JpulnJcaPgSaj~j~RPKE9;W=e1o|PRQ(+K_?fNpGF#AWY.X-GXJ(X;]' );
define( 'AUTH_SALT',        '0H:<l#EI+XDj<G&L:wvMJz=W{Ik{t*/;3x(XA!$lJwP;6zznjwOCu=7XU;HC#_*X' );
define( 'SECURE_AUTH_SALT', 'KwyN?,U:&2+J>$q(e~K_2614U7G2AK1<w^kkOk%GmRV=86LdQ>czEQ2J1_!Kkvn3' );
define( 'LOGGED_IN_SALT',   '>h:YI6 T$->0OfbpM.gJZ!Wp3`#W0t_qGkJW~jDk(u_a>w!}EU=@5btpPm>Tg/!L' );
define( 'NONCE_SALT',       'KQ_]=VMPH!m92xRMEX,HEb}dDj49IL}/fu#U`!Jf,&.{u.NxJP#+6XZttpwEdi?~' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'ww_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
